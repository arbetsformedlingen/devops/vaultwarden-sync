<!--
SPDX-FileCopyrightText: 2023 Arbetsförmedlingen JobTech - The Swedish Public Employment Service's Investment in JobTech Development

SPDX-License-Identifier: CC0-1.0
-->

# Vaultwarden Sync

**Description**:  Vaultwarden Sync is a shell script that helps to synchronize files with Vaultwarden. It provides two modes: 'up' for uploading files and 'down' for downloading files.

![CLI demo](screenshots/cli.gif)

It most likely also works with Bitwarden, considering it uses the Bitwarden CLI.

**Important security note**: The credentials are cached in `$PWD/bw_login/$BW_VERSION/conf`, so remember to remove that directory once you are done.

## Table of Contents

- [Installation and Requirements](#installation-and-requirements)
- [Quickstart Instructions](#quick-start-instructions)
- [Usage](#usage)
- [Known Issues](#known-issues)
- [Support](#support)
- [Contributing](#contributing)
- [Development](#development)
- [License](#license)
- [Maintainers](#maintainers)
- [Credits and References](#credits-and-references)

## Installation and Requirements

First install the [Bitwarden CLI](https://bitwarden.com/help/cli/) and [jq](https://jqlang.github.io/jq/).

To use Vaultwarden Sync, you need to have a Unix-like operating system with a Bash shell. No installation is required, just download the `vaultwarden_sync.sh` script and make it executable:

```shell
$ chmod +x vaultwarden_sync.sh
```

## Quick start instructions

To run Vaultwarden Sync, use the following command:

```shell
$ ./vaultwarden_sync.sh --org org upload
```

## Usage

To use Vaultwarden Sync, you need to provide the mode as the last argument. The mode can be either 'up' for uploading files or 'down' for downloading files. You can also specify the organization, collection, source base directory, Vaultwarden host, and Vaultwarden user using the corresponding options:

```shell
$ ./vaultwarden_sync.sh --org ORG --collection COLLECTION --source-base-dir DIR --vw-host HOST --vw-user USER MODE
```

If you wonder about terminology like [organisations](https://bitwarden.com/help/getting-started-organizations/) etc, check out the [Bitwarden docs](https://bitwarden.com/help/).


### All options:
```
Usage: ./vaultwarden_sync.sh [OPTIONS] MODE
Sync files with Vaultwarden.

Options:
  --org ORG                  Set the organization (default: joblinks)
  --collection COLLECTION    Set the collection (default: joblinks)
  --source-base-dir DIR      Set the source base directory (default: secrets)
  --vw-host HOST             Set the Vaultwarden host (default: https://your-vaultwarden-server)
  --vw-user USER             Set the Vaultwarden user (default: user@mail.com)

Mode:
  up                         Sync up
  down                       Sync down
```


## Known issues

This program probably has a lot of issues, as it has not been tested very much.

## Support

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Contributing

Please make merge requests!

----

## License

This project is licensed under the Apache License v2.0 License - see the [LICENSE](LICENSE) file for details

----

## Maintainers

See [CODEOWNERS](CODEOWNERS)
