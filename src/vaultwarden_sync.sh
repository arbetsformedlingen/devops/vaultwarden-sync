#!/bin/bash
set -euo pipefail


# Default values
ORG="joblinks"
COLLECTION="joblinks"
SOURCE_BASE_DIR="secrets"
VW_HOST="https://your-vaultwarden-server"
VW_USER="user@mail.com"
MODE=""


# Argument parsing
PARAMS=""
while (( "$#" )); do
  case "$1" in
    --org)
      ORG="$2"
      shift 2
      ;;
    --collection)
      COLLECTION="$2"
      shift 2
      ;;
    --source-base-dir)
      SOURCE_BASE_DIR="$2"
      shift 2
      ;;
    --vw-host)
      VW_HOST="$2"
      shift 2
      ;;
    --vw-user)
      VW_USER="$2"
      shift 2
      ;;
    --help)
      echo "Usage: $0 [OPTIONS] MODE"
      echo "Sync files with Vaultwarden."
      echo ""
      echo "Options:"
      echo "  --org ORG                  Set the organization (default: $ORG)"
      echo "  --collection COLLECTION    Set the collection (default: $COLLECTION)"
      echo "  --source-base-dir DIR      Set the source base directory (default: $SOURCE_BASE_DIR)"
      echo "  --vw-host HOST             Set the Vaultwarden host (default: $VW_HOST)"
      echo "  --vw-user USER             Set the Vaultwarden user (default: $VW_USER)"
      echo ""
      echo "Mode:"
      echo "  up                         Sync up"
      echo "  down                       Sync down"
      exit 0
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done


# Set positional arguments back if they were not used
eval set -- "$PARAMS"

# The last argument is the mode
MODE="${1:-}"

# Check if mode is set and is either 'up' or 'down'
if [[ -z "$MODE" || ! "$MODE" =~ ^(up|down|status)$ ]]; then
  echo "Error: Mode is not set or invalid. It should be either 'up', 'down' or 'status'." >&2
  exit 1
fi

if [ ! -d "$SOURCE_BASE_DIR" ]; then
    echo "Error: Source base directory '$SOURCE_BASE_DIR' does not exist." >&2
    exit 1
fi


for tool in bw jq; do
    command -v "${tool}" >/dev/null || { echo "*** please install ${tool}" >&2; exit 1; }
done


# Some global vars need to be set
BW_VERSION=$(bw --version)
SERVER_SYMBOL=$(echo "$VW_HOST" | tr -dc '[:alnum:]')
CONFDIR="$PWD/bw_login/$BW_VERSION/$SERVER_SYMBOL/conf"
mkdir -p "$CONFDIR"
export BITWARDENCLI_APPDATA_DIR="$CONFDIR"



## login, and cache token
if [ $(bw status --raw | jq -r .status) != "unlocked" ]; then
    if ! . "$CONFDIR/env.sh" || [ $(bw status --raw | jq -r .status) != "unlocked" ]; then
        bw config server "$VW_HOST"
        export BW_SESSION=$(bw login --method 0 --raw "$VW_USER")
        echo "export BW_SESSION='$BW_SESSION'" > "$CONFDIR/env.sh"
    fi
fi


function get_org_id() {
    local ORG="$1"

    # Get ID of organisation
    local ORG_ID=$(bw list organizations | jq -r '.[] | select(.name=="'$ORG'") | .id')
    if [ -z "$ORG_ID" ]; then
        echo "Error: Organisation '$ORG' not found." >&2
        exit 1
    fi
    echo "$ORG_ID"
}


function get_coll_id() {
    local COLLECTION="$1"
    local ORG_ID="$2"

    ## Get ID of collection
    local COLL_ID=$(bw list collections | jq -r '.[] | select(.name=="'$COLLECTION'" and .organizationId=="'$ORG_ID'") | .id')
    if [ -z "$COLL_ID" ]; then
        echo "Error: Collection '$COLLECTION' not found." >&2
        exit 1
    fi
    echo "$COLL_ID"
}


## remove group read permissions and sync on exit (this may fail if login failed)
trap "chmod -R go= '$SOURCE_BASE_DIR' ; bw sync" EXIT



function status() {
    if ! bw list organizations >/dev/null; then
        echo "ERROR: cannot perform bw command" >&2
        exit 1
    fi
    echo "INFO: status ok" >&2
}


function sync_down() {
    local TARGETDIR="$1"
    local ORG_ID="$2"
    local COLL_ID="$3"

    bw list items \
        | jq --arg COLL_ID "$COLL_ID" -r 'map(select( (.collectionIds | index($COLL_ID) // false) and (.name | contains(".synced") ) )) | .[].id'\
        | while read -r id; do
        local name=$(bw get item "$id" | jq -r '.name' | sed 's|.synced||')

        # Split name into directory parts and filename part
        local dir=""
        local fname="$name"
        while [[ "$fname" == *'__'* ]]; do
            local part="${fname%%__*}"   # Get directory part
            local dir="$dir/$part"       # Append directory part to the current directory
            local fname="${fname#*__}"   # Update filename part
        done
        local dir="${dir#/}"  # Remove leading slash if present
        if [ -z "$dir" ]; then
            echo "INFO: saving $fname" >&2
        else
            echo "INFO: saving $dir/$fname" >&2
        fi

        mkdir -p $TARGETDIR/$dir
        (
            cd $TARGETDIR/$dir || exit
            bw get --organizationid "$ORG_ID" notes "$id" | base64 -d > "$fname"
        )
    done
}



function sync_up() {
    local SOURCE_BASE_DIR="$1"
    local ORG_ID="$2"
    local COLL_ID="$3"

    find "$SOURCE_BASE_DIR" -type f | while read -r fname; do
        # Extract the relative path and replace '/' with '__'
        local item_name=$(echo "${fname#$SOURCE_BASE_DIR/}" | sed 's|/|__|g').synced

        local contents="$(base64 -w 0 - < $fname)"

        ## Determine if secret exists
        local ITEM_ID=$(bw list items \
            | jq --arg COLL_ID "$COLL_ID" --arg ITEM_NAME $item_name -r 'map(select( (.collectionIds | index($COLL_ID) // false) and (.name | startswith($ITEM_NAME) ) )) | .[].id')

        echo "INFO: uploading $fname" >&2
        if [ -n "$ITEM_ID" ]; then
            bw get template item | jq '.type = 2 | .secureNote.type = 0 | .notes = "'"$contents"'" | .name = "'"$item_name"'"' | bw encode | bw edit item "$ITEM_ID" >/dev/null
        else
            local ITEM_ID=$(bw get template item | jq '.type = 2 | .secureNote.type = 0 | .notes = "'"$contents"'" | .name = "'"$item_name"'"' | bw encode | bw create item | jq -r '.id')
        fi

        ## Move to coll/org
        echo "[\"$COLL_ID\"]" | bw encode |  bw move "$ITEM_ID" "$ORG_ID" >/dev/null || true
    done
}


case "$MODE" in
    up)
        bw sync
        ORG_ID=$(get_org_id "$ORG")
        sync_up "$SOURCE_BASE_DIR" "$ORG_ID" $(get_coll_id "$COLLECTION" "$ORG_ID")
        ;;
    down)
        bw sync
        ORG_ID=$(get_org_id "$ORG")
        sync_down "$SOURCE_BASE_DIR" "$ORG_ID" $(get_coll_id "$COLLECTION" "$ORG_ID")
        ;;
    status)
        status
        ;;
    *)
        echo "Error: Invalid mode. It should be either 'up' or 'down'."  >&2
        exit 1
        ;;
esac
